function peopleStep(simulation)
%A peoplestep. People will take one step and try to reach the exit.
s = simulation;

i=1;
while (i <= size(s.people,1))
    estimatedLocation = getEstimatedLocation(s,i);
    
    currentArea = s.areas(estimatedLocation(1), estimatedLocation(2));
    
    %Test if there is no plan or if the cost of the plan has changed
    %(knowledge update)
    if (size(s.people{i,4},1) == 0 || planCostChanged(s,i))
        s.people{i,4} = findRandomPathToExit(s,i);
        
        %test if the next area has been reached
    elseif (hasReached(s,estimatedLocation,true,false,s.people{i,4}(1,:)))
        s.people{i,4} = s.people{i,4}(2:end,:);        
    end
    
    %If the plan is empty, we are already at a exit node (we think (estimated location))
    if (size(s.people{i,4},1) == 0)
        goal = currentArea;
    else
        goal =  s.people{i,4}(1,:);
    end
    
    %Test if our goal has changed or if we should change our path for some
    %other reason
    if (goal ~= s.people{i,6} || size(s.people{i,7},1) == 0 || shouldChangePath(s,i))
        s.people{i,6} = goal;
        
        %Remove reservations
        if (size(s.people{i,7},1) > 0)
            pos = s.people{i,7}(1,:);
            s.reservations(pos(1),pos(2)) = false;
        end
        pos = s.people{i,1};
        s.reservations(pos(1),pos(2)) = false;
        
        s.people{i,7} = findPathInArea(s, pos, goal);
    end
    
    if (size(s.people{i,7},1) > 0)
        %Move
        from = s.people{i,1};
        to = s.people{i,7}(1,:);
        dif = double(to) - double(from);
        s.reservations(from(1),from(2)) = false;
        s.people{i,1} = to;
        s.people{1,2}(1:2) = s.people{1,2}(1:2) + dif;
        
        %Update path
        s.people{i,7} = s.people{i,7}(2:end,:);
        
        %Update reservations
        s.reservations(to(1),to(2)) = true;
        if (size(s.people{i,7},1) > 0)
            pos = s.people{i,7}(1,:);
            s.reservations(pos(1),pos(2)) = true;
        end
    else
        pos = s.people{i,1};
        s.reservations(pos(1),pos(2)) = true;
    end
    
    %Check if we reached the exit (This can not be placed in the move-if, as we
    %could have been placed directly at the exit
    if (hasReached(s,s.people{i,1},false,true))
        %Remove reservations
        if (size(s.people{i,7},1) > 0)
            pos = s.people{i,7}(1,:);
            s.reservations(pos(1),pos(2)) = false;
        end
        pos = s.people{i,1};
        s.reservations(pos(1),pos(2)) = false;
        
        %Remove self and add time to escaped list
        s.people(i,:) = [];
        s.escaped(size(s.escaped,1)+1,1) = s.time;
        i = i - 1;
    end
    
    i = i + 1;
end


end

