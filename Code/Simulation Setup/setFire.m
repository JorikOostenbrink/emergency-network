function fireMatrix = setFire(floorplan,detectors,areas,exits,radius)
%Place a fire at a random location around a random detector (within detection range)
%, the fire will not be in an exit area
fireMatrix = false(size(floorplan,1),size(floorplan,2));

%It might be better to construct a matrix with all acceptable locations and
%choose one at random, but I think this is faster
detector = unidrnd(size(detectors,1));
x = unidrnd(2*radius) - radius;
yMax = floor(sqrt(radius^2-x^2));
if (yMax == 0)
    y = 0;
else
    y = unidrnd(2*yMax) - yMax;
end
pos = detectors{detector,1} + [y x];

%I wish there was a do while in matlab
while (pos(1) <= 0 || pos(2) <= 0 || pos(1) >= size(floorplan,1) || pos(2) >= size(floorplan,2) || floorplan(pos(1),pos(2)) && ~ismember(areas(pos(1),pos(2)), exits))
    detector = unidrnd(size(detectors,1));
    x = unidrnd(2*radius) - radius;
    yMax = floor(sqrt(radius^2-x^2));
    if (yMax == 0)
        y = 0;
    else
        y = unidrnd(2*yMax) - yMax;
    end
    pos = detectors{detector,1} + [y x];
end

fireMatrix(pos(1),pos(2)) = 1;

