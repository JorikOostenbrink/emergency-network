function scaledImage = getNicePicture(simulation)
s = simulation;

scaledImage = s.floorplan + 2*s.fire;

for i=1:size(s.nodes,1)
   pos = s.nodes(i,1:2);
   scaledImage(pos(1),pos(2)) = 3;
end

for i=1:size(s.people,1)
   pos = s.people{i,1};
   scaledImage(pos(1),pos(2)) = 4;   
end
end

