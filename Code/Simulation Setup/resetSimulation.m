function simulation = resetSimulation(simulation,numPeople)
%Resets the simulation. i.e. makes a new simulation for the same floorplan
%+ areas + detectors
s = simulation;
simulation = createSimulation(s.floorplan, s.nodes, s.edges, s.areas, s.exits, s.detectors, numPeople);
end

