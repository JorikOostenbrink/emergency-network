classdef Simulation < handle
   properties
      floorplan
      nodes
      edges
      areas
      exits
      detectors
      people
      fire
      reservations
      time
      escaped
      died
   end
   methods
      function obj = Simulation(floorplan,nodes,edges,areas,exits,detectors,people,fire)
         obj.floorplan = floorplan;
         obj.nodes = nodes;
         obj.edges = edges;
         obj.areas = areas;
         obj.exits = exits;
         obj.detectors = detectors;
         obj.people = people;
         obj.fire = fire;
         obj.reservations = floorplan;
         obj.time = 0;
         obj.escaped = [];
         obj.died = [];
      end
   end
end

