function done = simulationStep(simulation)
timePerStep = 0.25;

simulation.time = simulation.time + timePerStep;

networkStep(simulation);

peopleStep(simulation);

spreadFire(simulation);

detectFire(simulation);

done = size(simulation.people,1) == 0;
end

