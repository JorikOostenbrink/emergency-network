function estimatedLocation = getEstimatedLocation(simulation,person)
%Returns the estimated location of person (person must be an index of
%simulation.people
s = simulation;
p = person;

% estimatedLocation = s.people{p,2}(1:2);
% estimatedLocation(1) = max(1,round(estimatedLocation(1)));
% estimatedLocation(2) = max(1,round(estimatedLocation(2)));
% estimatedLocation(1) = min(size(s.floorplan,1),estimatedLocation(1));
% estimatedLocation(2) = min(size(s.floorplan,2),estimatedLocation(2));
% 
% %Check if not in wall
% i=1;
% current = estimatedLocation;
% while (s.floorplan(current(1),current(2)))
%     test = [-i i];
%     for y = test
%     if (y > 0 && y <= size(s.floorplan,1))
%         x = max(1,estimatedLocation(2)-i);
%         current = [y x];
%         while (s.floorplan(current(1),current(2)) && x < i && x < size(s.floorplan,2))
%             x = x + 1;
%             current = [y x];
%         end
%     end
%     end
%     test = [-i i];
%     for x = test
%     if (x > 0 && x <= size(s.floorplan,2))
%         y = max(1,estimatedLocation(1)-i);
%         current = [y x];
%         while (s.floorplan(current(1),current(2)) && y < i && y < size(s.floorplan,1))
%             y = y + 1;
%             current = [y x];
%         end
%     end
%     end
%     i = i + 1;
% end
% estimatedLocation = current;

%We just return the real location, as trilateration is not precise enough
estimatedLocation = s.people{person,1};

end

