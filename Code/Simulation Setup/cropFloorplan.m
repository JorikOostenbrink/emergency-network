function [floorplan] = cropFloorplan(floorplan)
%Remove unnecesary open borders of floorplan
y = 1;
while(sum(floorplan(y,:)) == 0)
    y = y + 1;
end

floorplan = floorplan(y:end,:);

y = size(floorplan,1);
while(sum(floorplan(y,:)) == 0)
    y = y - 1;
end

floorplan = floorplan(1:y,:);

x = 1;
while(sum(floorplan(:,x)) == 0)
    x = x + 1;
end

floorplan = floorplan(:,x:end);

x = size(floorplan,2);
while(sum(floorplan(:,x)) == 0)
    x = x - 1;
end

floorplan = floorplan(:,1:x);
end

