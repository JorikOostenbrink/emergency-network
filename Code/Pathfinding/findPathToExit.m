function [path] = findPathToExit(simulation, person, extraCosts)
%Finds the (optimal) path to the exit (over areas/nodes) for person, possible adding
%extraCosts costs. person should be an index for simulation.people.
s = simulation;
p = person;

costs = s.people{p,3};

if (exist('extraCosts', 'var'))
    costs = costs + extraCosts;
end

eL = getEstimatedLocation(s,p);
startNode = s.areas(eL(1),eL(2));

visited = false(size(s.nodes,1),1);
distances = inf(size(s.nodes,1),1);
prev = zeros(size(s.nodes,1),1,'uint16');
distances(startNode) = 0;
current = startNode;

while (current ~= inf && ~ismember(current, s.exits))
    visited(current) = true;
    
    for i = 1:size(s.edges,2)
        dist = distances(current) + costs(i) + s.edges(current,i);
        if (dist < distances(i,1))
            distances(i,1) = dist;
            prev(i) = current;
        end
    end
    
    min = inf;
    current = inf;
    
    for i = 1:size(distances,1)
        if (~visited(i) && distances(i) < min)
            min = distances(i);
            current = i;
        end
    end
end

if (current == inf)
    fprintf('Could not find path to exit from node %u\n', startNode);
    path = -1;
else    
    path = zeros(0,1,'uint16');
    index = 1;
    
    while(current ~= startNode)
       path(index,1) = current;
       index = index + 1;
       current = prev(current);
    end
    
    path = flipud(path);
end
end

