function [floorplanOut] = scaleFP(floorplanIn, width, height)
%[floorplanOut] = scaleFP(floorplanIn, width, height)
%Scales a floorplan of a floor that is width meters wide and with a
%length/height of height meters to a floorplan where every pixel is 0.5x0.5 meters in reality 

pixelWidth = 0.5;
pixelHeight = 0.5;

rows = floor(height/pixelHeight);
columns = floor(width/pixelWidth);

floorplanOut = false(rows, columns);

stepX = size(floorplanIn, 2) / columns;
stepY = size(floorplanIn, 1) / rows;

%Combines multiple pixels into 1 pixel or transforms 1 pixel to multiple
%pixels. If even 1 of the used input pixels is a wall, every resulting
%output pixel is also a wall
for yOut = 0:(rows-1)
    for xOut = 0:(columns-1)
        for yIn = floor(yOut*stepY):(floor(yOut*stepY + max(0,stepY-1)))
            for xIn = floor(xOut*stepX):(floor(xOut*stepX + max(0,stepX-1)))
                floorplanOut(yOut+1, xOut+1) = max(floorplanOut(yOut+1, xOut+1), floorplanIn(yIn+1, xIn+1));
            end
        end
    end
end

