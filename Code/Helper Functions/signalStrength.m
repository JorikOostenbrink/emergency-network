function receivedPower = signalStrength (APlocation, measuredLocation)
%Returns signalstrength for transmitting from APlocation to
%measuredLocation (both 1x2 position vectors)

%Taking assumption antennaGain=0 and there's no other losses except the
%space loss
Grx = 0;
Gtx = 0;
transmitPower = -10;

%set the frequency (in GHz)
f = 2.4;

%pixel in meter
pixelinmeter=0.5;

%define the path-loss exponent
n = 5;

%define log-normal shadowing Standard Deviation
sigma = 8;

%calculate the distance between two points
distance = pdist (double([APlocation; measuredLocation]))*pixelinmeter;

%give the shadowing effect with log-normal distribution
X = normrnd (0, sigma);
%calculate the received power at measuredLocation
receivedPower = transmitPower - Gtx - Grx + (20*log10(0.3/(4*pi*f)) - 10*n*log10 (distance) + X);
end
