function [ret] = AAfastIsMember(B,A)
%Based on http://www.mathworks.com/matlabcentral/answers/51102-ismember-function-too-slow
%Do not use outside its specific use case (2 matrices full of integer positions)
if (~isempty(A)&&~isempty(B))
    maxY = max(max(A(:,1)),max(B(:,1)));
    maxX = max(max(A(:,2)),max(B(:,2)));
    test = false(1,maxY+maxY*maxX);
    
    AC = A(:,1) + maxY*A(:,2);
    BC = B(:,1) + maxY*B(:,2);
    
    test(AC) = 1;
    ret = test(BC);
else
    ret = zeros(1,size(B,1));
end
end

