function estimatedLocation = trilateration (APLocation, receivedPower)
%based on wikipedia (https://en.wikipedia.org/wiki/Trilateration)
%Uses trilateration to estimate a location based on signal strength.
%APLocation should be a 3x2 matrix containing position vectors.

pixelinmeter = 0.5;
distance = distanceEstimation (receivedPower)/pixelinmeter;

da = distance(1);
db = distance(2);
dc = distance(3);

ex = (APLocation(2,:) - APLocation(1,:))/(pdist(APLocation(1:2,:)));
i = dot(ex,(APLocation(3,:)-APLocation(1,:)));
ey = (APLocation(3,:)-APLocation(1,:)-i*ex)/(norm(APLocation(3,:) - APLocation(1,:) -i*ex));

d = pdist(APLocation(1:2,:));
j = dot(ey,(APLocation(3,:)-APLocation(1,:)));

x = (da^2-db^2+d^2)/(2*d);
y = (da^2-dc^2+i^2+j^2)/(2*j) - i/j*x;

estimatedLocation = APLocation(1,:) + x*ex + y*ey;
end