%Test script

numPeople = 5;
n = 20;

rng('shuffle');

floorplan = convertFloorplan(imread('floorplanA.jpg'), 0.05, 25, 20);
%floorplan = convertFloorplan(imread('Floorplan.jpg'), 0.4, 40, 28);

[nodes, edges ,areas, exits] = AAassignAreas(floorplan);

detectors = nodesAsDetectors(nodes);

numEscaped = zeros(n,1);
averageEscapeTime = zeros(n,1);
medianEscapeTime = zeros(n,1);
firstEscapeTime = zeros(n,1);
lastEscapeTime = zeros(n,1);

for i = 1:n
    i
    simulation = createSimulation(floorplan, nodes, edges, areas, exits, detectors, numPeople);
    
    while(~simulationStep(simulation) && simulation.time <= 900);        
    end
    
    numEscaped(i,1) = size(simulation.escaped,1);
    averageEscapeTime(i,1) = mean(simulation.escaped);
    medianEscapeTime(i,1) = median(simulation.escaped);
    if (numEscaped(i,1) > 0)
       firstEscapeTime(i,1) = simulation.escaped(1,1);
       lastEscapeTime(i,1) = simulation.escaped(end,1);
    else
       firstEscapeTime(i,1) = Inf;
       lastEscapeTime(i,1) = Inf;
    end
end