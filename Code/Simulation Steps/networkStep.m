function networkStep(simulation)
%A network step. People will update their knowledge and estimation of
%location
s = simulation;

%Fill old knowledge
for i=1:size(s.people,1)
    s.people{i,5} = s.people{i,3};
end

for i=1:size(s.people,1)
    cDetectors = getConnected(s.detectors,s.people{i,1});
    
    %Estimate location
    if (size(cDetectors,1) >= 3)
        trio = randperm(size(cDetectors,1),3);
        
        locations = zeros(3,2);
        signals = zeros(3,1);
        
        for j=1:3
            locations(j,:) = s.detectors{cDetectors(trio(j),1),1};
            signals(j,1) = cDetectors(trio(j),2);
        end
        
        estimatedLocation = trilateration(locations, signals);
        
        if (isfinite(estimatedLocation) == [1 1])
            estLoc = s.people{i,2};
            estLoc(3) = estLoc(3) + 1;
            estLoc(1:2) = estLoc(1:2)*(estLoc(3)-1)/estLoc(3) + estimatedLocation(1:2)/estLoc(3);            
            s.people{i,2} = estLoc;
        end
    elseif (s.people{i,2}(3) == 0 && size(cDetectors,1) > 0)
        estimatedLocation = [0 0];
        
        for j=1:size(cDetectors,1)
            estimatedLocation = estimatedLocation + s.detectors{cDetectors(j,1),1};
        end
        
        s.people{i,2} = [estimatedLocation / size(cDetectors,1) 0];
    end
    
    %Update knowledge from detectors
    for j=1:size(cDetectors,1)
        detector = [s.detectors{cDetectors(j,1),1} s.detectors{cDetectors(j,1),2}];
        area = s.areas(detector(1),detector(2));
        
        s.people{i,3}(area,1) = max(s.people{i,3}(area,1), detector(3));
    end
    
    cPeople = getConnected(s.people,s.people{i,1});
    cPeople = cPeople(:,1);
    
    %Update knowledge from people
    for j=1:size(cPeople,1)
       s.people{i,3} = max(s.people{i,3}, s.people{cPeople(j,1),5}); 
    end
end
end

