function path = findPathInArea(simulation, from, goal)
%Find a path from from (1x2 position vector) to goal (goal area)
%If from is already in goal, path will be a path from from to 
%the exit of the floorplan (if the area is an exit area) or the center of
%the area (otherwise)

maxT = 50;

s = simulation;

toArea = true;
toExit = false;

if (~exist('goal', 'var') || goal == s.areas(from(1),from(2)))
    goal = s.areas(from(1),from(2));
    toArea = false;
    
    if (ismember(s.areas(from(1),from(2)),s.exits))
        toExit = true;
    end
end

visited = false(size(s.floorplan,1),size(s.floorplan,2),maxT);
prev = zeros(size(s.floorplan,1),size(s.floorplan,2),maxT,2,'uint16');

current = [from 1];

while (current(1) ~= -1 && ~hasReached(s,current,toArea,toExit,goal))
    visited(current(1),current(2),current(3)) = true;
    
    if (current(1) > 1)
        prev(current(1)-1,current(2),current(3)+1,:) = [current(1),current(2)];
    end
    if (current(1) < size(s.floorplan,1))
        prev(current(1)+1,current(2),current(3)+1,:) = [current(1),current(2)];
    end
    if (current(2) > 1)
        prev(current(1),current(2)-1,current(3)+1,:) = [current(1),current(2)];
    end
    if (current(2) < size(s.floorplan,2))
        prev(current(1),current(2)+1,current(3)+1,:) = [current(1),current(2)];
    end
    
    prev(current(1),current(2),current(3)+1,:) = [current(1),current(2)];
    
    t = current(3);
    current = -1;
    
    while (current(1) == -1 && t <= maxT)
        y = 1;
        while (current(1) == -1 && y <= size(s.floorplan,1))
            x = 1;
            while (current(1) == -1 && x <= size(s.floorplan,2))
                if (~s.floorplan(y,x) && prev(y,x,t,1) > 0 && ~visited(y,x,t) && (t > 3 || ~s.reservations(y,x)) && ~s.fire(y,x))
                    current = [y x t];
                end
                
                x = x+1;
            end
            y = y+1;
        end
        t = t+1;
    end
end

path = zeros(0,2,'uint16');
index = 1;

if (current == -1)
    fprintf('Could not find path from (%u, %u)\n', from(1),from(2));
    path = [from(1) from(2)];
else
    while (current(3) > 1)
        path(index,1:2) = current(1:2);
        index = index + 1;
        current(1:2) = prev(current(1),current(2),current(3),1:2);
        current(3) = current(3) - 1;
    end
    
    path = flipud(path);
end
end