function people = placeUniformPeople(floorPlan, fireMatrix, numNodes, numPeople)
%Return a cell of people. The people will be placed on random uniformly
%chosen positions which are not on fire (or a wall)
peopleMatrix = false(size(floorPlan,1),size(floorPlan,2));

people = cell(numPeople,7);

for i = 1:numPeople
    pos = unidrnd([size(floorPlan,1) size(floorPlan,2)]);
    
    while (floorPlan(pos(1),pos(2)) || peopleMatrix(pos(1),pos(2)) || fireMatrix(pos(1),pos(2)))
        pos = unidrnd([size(floorPlan,1) size(floorPlan,2)]);
    end
    
    peopleMatrix(pos(1),pos(2)) = 1;
    people{i,1} = pos; %Real position
    people{i,2} = [0 0 0]; %Estimated position + multiplier
    people{i,3} = zeros(numNodes,1); %Knowledge
    people{i,4} = zeros(0,0); %Plan (path to reach exit (through nodes))
    people{i,5} = zeros(numNodes,1); %(One step) old knowledge (used to check if plan needs updating)
    people{i,6} = 0; %Goal: area currently trying to reach
    people{i,7} = zeros(0,0); %Path to reach said goal
end

end

