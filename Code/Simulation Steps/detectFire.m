function detectFire(simulation)
%Detection step. All detectors update their penalties.
radius = 10; %Detection radius

s = simulation;
penalty = size(s.floorplan,1) + size(s.floorplan,2);

for i = 1:size(s.detectors,1)
    detector = s.detectors{i,1};
    
    [x y] = meshgrid(1:size(s.fire,2),1:size(s.fire,1));
    C = sqrt((x-detector(1,2)).^2+(y-detector(1,1)).^2) <= radius & s.fire;
    
    detected = sum(sum(C));
        
    s.detectors{i,2} = detected*penalty;
end

end

