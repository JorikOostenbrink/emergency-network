function [con] = getConnected(transmitters, pos)
%Returns all transmitters in range of pos
%Transmitters should be  a cell of transmitters (people or detectors)
%Pos should be a 1x2 position vector
%Con is a nx2 matrix containing all transmitters in range and their signal
%strength
receiverStrength = -100;

con = zeros(0,2);
index = 1;

for i=1:size(transmitters,1)
    receivedPower = signalStrength (transmitters{i,1}, pos);
    
    if (receivedPower >= receiverStrength)
        con(index,1:2) = [i receivedPower];
        index = index + 1;
    end
end
end

