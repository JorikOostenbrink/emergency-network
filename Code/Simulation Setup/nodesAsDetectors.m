function [detectors] = nodesAsDetectors(nodes)
%Returns a cell of detectors, with a detector placed on every node
detectors = cell(size(nodes,1),2);

for i = 1:size(nodes,1)
   detectors{i,1} = nodes(i,1:2);
   detectors{i,2} = 0;%Fire penalty
end

end

