function path = findRandomPathToExit(simulation,person)
%Finds the (not always optimal) path to the exit (over areas/nodes) for person.
%person should be an index for simulation.people.
s = simulation;
p = person;

prob = 1/4; %Probability of choosing a not optimal path

optimal = findPathToExit(simulation, person);

if (rand() <= prob)
    costs = zeros(size(s.nodes,1),1);
    
    pos = getEstimatedLocation(s,p);
    current = s.areas(pos(1),pos(2));
    for i=1:size(optimal,1)        
        costs(i,1) = s.edges(current,i) + s.people{p,3}(i); %Double cost over optimal path
    end
    
    path = findPathToExit(simulation, person, costs);    
else
    path = optimal;
end
end

