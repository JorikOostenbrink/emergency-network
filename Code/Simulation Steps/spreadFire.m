function spreadFire(simulation)
%One spread fire step. Fire spreads randomly across the map.
probability = 2/500; %This should be equal to 8mm/s

s = simulation;

spread = rand(size(s.fire,1),size(s.fire,2),4) < probability;
%:,:,1 is fire comes from left
%,2 is fire comes from right
%,3 is fire comes from up
%,4 is fire comes from down

s.fire = s.fire | ([zeros(size(s.fire,1),1) s.fire(:,1:end-1)] & spread(:,:,1))...
    | ([s.fire(:,2:end) zeros(size(s.fire,1),1) ] & spread(:,:,2))...
    | ([zeros(1,size(s.fire,2));s.fire(1:end-1,:)] & spread(:,:,3))...
    | ([s.fire(2:end,:);zeros(1,size(s.fire,2))] & spread(:,:,4));

s.fire = s.fire & ~s.floorplan;

i = 1;
while(i <= size(s.people,1))
    if (s.fire(s.people{i,1}(1),s.people{i,1}(2)))
       s.died(size(s.died,1)+1,:) = [s.time];
       s.people(i,:) = [];
    else
       i = i + 1; 
    end
end
end

