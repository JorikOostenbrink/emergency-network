function changed = planCostChanged(simulation,person)
%Returns true iff the cost of the plan of person has changed (by a
%knowledge update). person must be an index of simulation.people
s = simulation;
p = person;

changed = false;
i = 1;

while (~changed && i <= size(s.people{p,4},1))
    changed = changed | (s.people{p,3}(s.people{p,4}(i,1),:) ~= s.people{p,5}(s.people{p,4}(i,1),:));
    i = i + 1;
end
end

