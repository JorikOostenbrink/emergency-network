function [connections, dists] = AAgetConnections(floorplan,p)
%Returns every point on the floorplan that is in range of p and the
%distances to every node which is in range or nearly in range (for creating edges later on).
c = 10;

dists = zeros(0,3);
index = 1;

visited = floorplan;
distances = inf(size(floorplan,1),size(floorplan,2));
distances(p(1),p(2)) = 0;

current = p;

while(distances(current(1),current(2)) <= c+1 && ~visited(current(1),current(2)))
    dists(index, :) = [current distances(current(1),current(2))];
    index = index + 1;
    visited(current(1),current(2)) = 1;
    
    dist = distances(current(1),current(2)) + 1;
    
    if (current(1) > 1)
        if (dist < distances(current(1)-1,current(2)))
            distances(current(1)-1,current(2)) = dist;
        end
    end
    if (current(1) < size(floorplan,1))
        if (dist < distances(current(1)+1,current(2)))
            distances(current(1)+1,current(2)) = dist;
        end
    end
    if (current(2) > 1)
        if (dist < distances(current(1),current(2)-1))
            distances(current(1),current(2)-1) = dist;
        end
    end
    if (current(2) < size(floorplan,2))
        if (dist < distances(current(1),current(2)+1))
            distances(current(1),current(2)+1) = dist;
        end
    end
    
    d = distances;
    d(visited) = Inf;
    [~, I] = min(d(:));
    [current(1), current(2)] = fastInd2Sub(size(d),I);
end

connections = dists(dists(:,3) <= c,1:2);
end

