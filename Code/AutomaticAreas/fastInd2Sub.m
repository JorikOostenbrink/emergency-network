function [y, x] = fastInd2Sub(size,I)
%Copied from http://tipstrickshowtos.blogspot.nl/2011/09/fast-replacement-for-ind2sub.html
rows = size(1);
%cols = size(2); Not actually necessary

y = rem(I-1,rows)+1;
x = (I-y)/rows + 1;

end

