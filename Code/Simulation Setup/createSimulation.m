function [simulation] = createSimulation(floorplan, nodes, edges, areas, exits, detectors, numPeople)
%Create a simulation object, set fire, place people
detectorRadius = 10;

fire = setFire(floorplan,detectors,areas,exits,detectorRadius);
people = placeUniformPeople(floorplan, fire, size(nodes,1), numPeople);

simulation = Simulation(floorplan,nodes,edges,areas,...
    exits,detectors,people,fire);

%Set up reservations
for i=1:size(simulation.people,1)
   pos = simulation.people{i,1};
   simulation.reservations(pos(1),pos(2)) = true;
end

detectFire(simulation);
end

