function varargout = finalGUI(varargin)
% FINALGUI MATLAB code for finalGUI.fig
%      FINALGUI, by itself, creates a new FINALGUI or raises the existing
%      singleton*.
%
%      H = FINALGUI returns the handle to a new FINALGUI or the handle to
%      the existing singleton*.
%
%      FINALGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FINALGUI.M with the given input arguments.
%
%      FINALGUI('Property','Value',...) creates a new FINALGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before finalGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to finalGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help finalGUI

% Last Modified by GUIDE v2.5 08-Nov-2015 11:59:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @finalGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @finalGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before finalGUI is made visible.
function finalGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to finalGUI (see VARARGIN)

% Choose default command line output for finalGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes finalGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = finalGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_X_Callback(hObject, eventdata, handles)
% hObject    handle to edit_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_X as text
%        str2double(get(hObject,'String')) returns contents of edit_X as a double


% --- Executes during object creation, after setting all properties.
function edit_X_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editY_Callback(hObject, eventdata, handles)
% hObject    handle to editY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editY as text
%        str2double(get(hObject,'String')) returns contents of editY as a double


% --- Executes during object creation, after setting all properties.
function editY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_BWLevel_Callback(hObject, eventdata, handles)
% hObject    handle to edit_BWLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_BWLevel as text
%        str2double(get(hObject,'String')) returns contents of edit_BWLevel as a double


% --- Executes during object creation, after setting all properties.
function edit_BWLevel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_BWLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_LoadImage.
function pushbutton_LoadImage_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_LoadImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

image_file = uigetfile('*.*');  
if ~isempty(image_file)
handles.im_original=imread(image_file); 
% set(handles.orgIm,'HandleVisibility','ON');  
% axes(handles.orgIm);
% image(handles.im_original);
% axis equal;
% axis tight;
% axis off;
% set(handles.orgIm,'HandleVisibility','OFF');

%show the image into axis1
axes(handles.axes1);
imshow(handles.im_original,[0 1 0; 1 0 1])
% imshow(handles.im_original, 'Parent', handles.axes1);

% handles.Scaled_fp = fp;
% handles.x = x;
% handles.y = y;
guidata(hObject, handles);  
end

% --- Executes on button press in pushbutton_ConvertImage.
function pushbutton_ConvertImage_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ConvertImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Get image size
[x,y]=size(handles.im_original(:,:,1));

%Get bw level input from GUI
bwlevel = get(handles.edit_BWLevel,'String');
handles.bwlevel_number = str2double(bwlevel);

%Get the dimensions input from GUI
floorplan_X = get(handles.edit_X,'String');

% %floorplan_Y = get(handles.edit4,'Double');
handles.floorplan_X_number = str2double(floorplan_X);
%floorplan_Y_number = str2double(floorplan_Y);

%Calculate aspect ratio
aspect_ratio = y ./ x;

%aspect_ratio = rats(y ./ x);
handles.floorplan_Y_number = aspect_ratio .* handles.floorplan_X_number;

%Start converting the floorplan
handles.Scaled_fp = convertFloorplan(handles.im_original,handles.bwlevel_number,handles.floorplan_Y_number,handles.floorplan_X_number);

%show the image into axis1
% imshow(~handles.Scaled_fp, 'Parent', handles.axes1);
axes(handles.axes1);
imshow(handles.Scaled_fp,[1 1 1;0 0 0])

guidata(hObject, handles);  



% --- Executes on button press in pushbutton_start.
function pushbutton_start_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Get input of number of people from GUI
numberofPeople = get(handles.edit_NumberofPeople,'String');
numPeople = str2double(numberofPeople);

[nodes, edges ,areas, exits] = AAassignAreas(handles.Scaled_fp);
detectors = nodesAsDetectors(nodes);

simulation = createSimulation(handles.Scaled_fp, nodes, edges, areas, exits, detectors, numPeople);
while(~simulationStep(simulation));
   axes(handles.axes2);
   imshow(getNicePicture(simulation)+1,[1 1 1;0 0 0;1 0 0;0 1 0;0 0 1])
   pause(0.1);   
end
imshow(getNicePicture(simulation)+1,[1 1 1;0 0 0;1 0 0;0 1 0;0 0 1])

function edit_NumberofPeople_Callback(hObject, eventdata, handles)
% hObject    handle to edit_NumberofPeople (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_NumberofPeople as text
%        str2double(get(hObject,'String')) returns contents of edit_NumberofPeople as a double


% --- Executes during object creation, after setting all properties.
function edit_NumberofPeople_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_NumberofPeople (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

