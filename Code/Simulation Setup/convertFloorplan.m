function [floorplan] = convertFloorplan(img, bwLevel, width, height)
%[floorplan] = convertFloorplan(img, bwLevel, width, height)
%Converts an image of a floorplan to a floorplan as used in this program
%bwLevel is the level to use in im2bw
%Width and height should be the width and length/height of the floor in
%meters

bwimg = im2bw(img, bwLevel);
bwimg = 1 - bwimg;

floorplan = bwareaopen(bwimg, 8);

floorplan = scaleFP(floorplan, width, height);

if (sum(sum(floorplan)) == 0)
   disp('invalid floorplan: floorplan is empty');
   floorplan = [];
else
   floorplan = cropFloorplan(floorplan); 
end
end

