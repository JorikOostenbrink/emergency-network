function distance = distanceEstimation (receivedPower)
%Estimate distance based on received signal strength

n = 5;
Gtx = 0;
Grx = 0;
f = 2.4;
transmitPower = -10;

% averageReceivedPower = sum(receivedPower,2)/size(receivedPower,2);

distance = (10.^-((receivedPower - transmitPower + Gtx + Grx - 20*log10(0.3/(4*pi*f)))/(10*n)));

end

