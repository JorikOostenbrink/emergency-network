function reached = hasReached(simulation,current,toArea,toExit,goal)
%Returns true iff current (a 1x2 position vector) has reached 
%the goal area (if toArea), the exit (if toExit) or the goal node (if neither) 
s = simulation;

if (toArea)
    reached = (s.areas(current(1),current(2)) == goal);
elseif(toExit)
    reached = (current(1) == 1 || current(2) == 1 || current(1) == size(s.floorplan,1) || current(2) == size(s.floorplan,2));
else
    node = s.nodes(goal,:);
    reached = (current(1) == node(1,1) && current(2) == node(1,2));
end
end

