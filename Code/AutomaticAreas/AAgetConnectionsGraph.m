function [graph] = AAgetConnectionsGraph(floorplan)
%Returns a cell containing all points in range of all points (and the
%distances between all points and the points in range or almost in range)
graph = cell(size(floorplan,1),size(floorplan,2),2);

for y = 1:size(floorplan,1)
    for x = 1:size(floorplan,2)
        [graph{y,x,1}, graph{y,x,2}] = AAgetConnections(floorplan,[y x]);
    end
end
end

