function should = shouldChangePath(simulation,person)
%Returns true iff person should recalculate its path (because of
%reservations or fire). person should be an index for simulation.people
s = simulation;
p = person;

should = false;
if (size(s.people{p,7},1) >= 2)
    pos = s.people{p,7}(2,:);
    should = s.reservations(pos(1),pos(2));
end
i = 1;

while(~should && i <= size(s.people{p,7},1))
    pos = s.people{p,7}(i,:);
    should = should | s.fire(pos(1),pos(2));
    i = i + 1;
end
end

