function [nodes, edges ,areas, exits] = AAassignAreas(floorplan)
%Divide the floorplan in areas

[nodes, distances] = AAgetNodes(floorplan);

areas = zeros(size(floorplan,1),size(floorplan,2),'uint16');
dMap = inf(size(floorplan,1),size(floorplan,2));
edges = inf(size(nodes,1),size(nodes,1));
exits = zeros(0,1,'uint16');
index = 1;

for i = 1:size(nodes,1)
    dist = distances{i,1};
    for j = 1:size(dist,1)
        if (dMap(dist(j,1),dist(j,2)) == inf)
            areas(dist(j,1),dist(j,2)) = i;
            dMap(dist(j,1),dist(j,2)) = dist(j,3);
        else
            neighbor = areas(dist(j,1),dist(j,2));
            d = min(edges(neighbor,i), dist(j,3) + dMap(dist(j,1),dist(j,2)));
            edges(i,neighbor) = d;
            edges(neighbor,i) = d;
            
            if (dMap(dist(j,1),dist(j,2)) > dist(j,3))
                areas(dist(j,1),dist(j,2)) = i;
                dMap(dist(j,1),dist(j,2)) = dist(j,3);
            end
        end
        if (dist(j,1) == 1 || dist(j,1) == size(floorplan,1) || dist(j,2) == 1 || dist(j,2) == size(floorplan,2))
            if (~ismember(i,exits))
                exits(index) = i;
                index = index + 1;
            end
        end
    end
end
end

