function [nodes, distances] = AAgetNodes(floorplan)
%Returns a valid node placement for the floorplan, and the distances from
%the nodes to all points in range or almost in range
graph = AAgetConnectionsGraph(floorplan);

connected = zeros(0,2);
indexC = 1;

nodes = zeros(0,2);
distances = cell(0,1);
indexN = 1;

totalElements = sum(~floorplan(:));

while (size(connected,1) < totalElements)
   max = 0;
   p = [-1 -1];
   
   for y = 1:size(floorplan,1)
       for x = 1:size(floorplan,2)
           con = graph{y,x,1};
           elements = sum(~AAfastIsMember(con,connected));
           
           if (elements > max)
               max = elements;
               p = [y x];
           end
       end
   end
   
   con = graph{p(1),p(2),1};
   con = con(~AAfastIsMember(con,connected),:);
   
   connected(indexC + (0:(size(con,1)-1)),1:2) = con;
   indexC = indexC + size(con,1);
   
   nodes(indexN,:) = p;
   distances{indexN,1} = graph{p(1),p(2),2};
   indexN = indexN + 1;
end

end

